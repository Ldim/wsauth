# README #

WebSocket "фреймфорк" для авторизации и обмена сообщениями на основе Json.

### Используемые технологии ###

* Wildfly 9.0.2
* Java8
* Ehcache(для управления токенами)
* Hibernate, HSQLDB(in memory)

### Установка и настройка ###

* Разверните скомпилированный .war-файл на сервер приложений
* Откройте в браузере http://localhost:8080/wsauth/
* Для настройки времени жизни токена используйте ehcache.xml в /WEB-INF/classes
* Для настройки формата даты используйте config.properties в /WEB-INF/classes
* Для формирования списка авторизируемых пользователей используйте import.sql в /WEB-INF/classes

### Описание ###

Приложение представляет собой framework состоящий из аннотаций @AuthorizeSession, @HandleError, @AuthenticateWithToken. Которые позволяют работать непосредственно с @ServerEndpoint при условии, что для endpoint определены encoders и decoders(*прим.:* 
```
#!java

@ServerEndpoint(value = "/service", encoders = { MessageEncoder.class }, decoders = { MessageDecoder.class })
```
 ), где MessageEncoder.class, MessageDecoder.class классы данного фреймворка.

**@AuthorizeSession** используется для аннотирования @OnMessage метода для фильтрации входящих сообщений с типом LOGIN_CUSTOMER с последующей авторизацией. А так же для проверки авторизации WebSocket ceccии в случае сообщений другого типа.

**@AuthenticateWithToken** используется для аннотирования @OnOpen метода для проверки токена и последующей авторизации сессии(в противном случае сессия автоматически закрывается). Ранее полученный токен передается клиентом как query string в запросе вида: *http://localhost:8080/wsauth/service?{token}*.

**@HandleError** используется для аннотирования @OnError метода для обработки ошибок endpointа. Причем ошибки класса фреймворка WebSocketException обрабатываются как CUSTOMER_ERROR с передачей исходного sequence_id, а все остальные(RuntimeException) как SERVER_ERROR c генерацией "рандомного" UUID для sequence_id.

### Тестовая страница ###

* **вверху:** нажмите Open.
* введите FOO@EMAIL.RU qwerty(из файла import.sql)
* нажмите History
* **внизу:** введите token полученный из логов
* нажмите Open
* нажмите History