package com.ldim.wsauth;

import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;

import com.ldim.wsauth.entities.Customer;
import com.ldim.wsauth.repositories.CustomerRepository;
import com.ldim.wsauth.security.ExternalAuthenticatorService;

@Stateless
public class ExternalAuthenticatorServiceImpl implements ExternalAuthenticatorService {
	@Inject
	CustomerRepository repository;
	
	@Override
	public boolean authenticate(String username, String password) {
		List<Customer> customers = repository.findByEmailAndPassword(username, password);
		return !customers.isEmpty();
	}

}
