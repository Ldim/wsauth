package com.ldim.wsauth;

import java.util.UUID;

import javax.ejb.Lock;
import javax.ejb.LockType;
import javax.ejb.Schedule;
import javax.ejb.Singleton;
import javax.ejb.Startup;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ldim.wsauth.model.DomainUser;
import com.ldim.wsauth.security.TokenService;

import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Element;

@Singleton
@Startup
public class TokenServiceImpl implements TokenService {
	private static final Logger log = LoggerFactory.getLogger(TokenService.class);
	private static final Cache websocketTokenCache = CacheManager.getInstance().getCache("websocketTokenCache");

	@Schedule(minute = "*/30", hour = "*")
	public void evictExpiredTokens() {
		log.info("Evicting expired tokens " + websocketTokenCache.getKeys().size());
		websocketTokenCache.evictExpiredElements();
	}

	@Override
	@Lock(LockType.READ)
	public String generateNewToken() {
		return UUID.randomUUID().toString();
	}

	@Override
	public long store(String token, DomainUser domainUser) {
		kill(domainUser);
		websocketTokenCache.put(new Element(token, domainUser));
		return websocketTokenCache.get(token).getExpirationTime();
	}

	@Override
	@Lock(LockType.READ)
	public boolean contains(String token) {
		return websocketTokenCache.get(token) != null;
	}

	@Override
	@Lock(LockType.READ)
	public DomainUser retrieve(String token) {
		return (DomainUser) websocketTokenCache.get(token).getObjectValue();
	}

	private void kill(DomainUser user) {
		Element element = websocketTokenCache.getAll(websocketTokenCache.getKeys()).values().stream()
				.filter(e -> ((DomainUser) e.getObjectValue()).equals(user)).findFirst().orElse(null);
		if (element != null) {
			websocketTokenCache.remove(element.getObjectKey());
		}
	}

}
