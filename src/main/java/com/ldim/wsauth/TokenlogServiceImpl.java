package com.ldim.wsauth;

import java.util.Date;

import javax.ejb.Stateless;
import javax.inject.Inject;

import com.ldim.wsauth.entities.Tokenlog;
import com.ldim.wsauth.model.DomainUser;
import com.ldim.wsauth.model.Message;
import com.ldim.wsauth.model.MessageType;
import com.ldim.wsauth.repositories.TokenlogRepository;
import com.ldim.wsauth.security.TokenlogService;

@Stateless
public class TokenlogServiceImpl implements TokenlogService {
	
	@Inject
	TokenlogRepository tokenlog;
	
	@Override
	public void write(DomainUser user, String token) {
		tokenlog.create(new Tokenlog(new Date(), user.getEmail(), token));
	}

	@Override
	public Message getLog(String sequenceId) {
		return new Message(MessageType.CUSTOMER_REQUEST, sequenceId, tokenlog.getAll());
	}

}
