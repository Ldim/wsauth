package com.ldim.wsauth.endpoints;

import java.io.IOException;

import javax.inject.Inject;
import javax.websocket.EncodeException;
import javax.websocket.OnClose;
import javax.websocket.OnError;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.ServerEndpoint;

import com.ldim.wsauth.model.Message;
import com.ldim.wsauth.model.MessageType;
import com.ldim.wsauth.security.AuthenticateWithToken;
import com.ldim.wsauth.security.HandleError;
import com.ldim.wsauth.security.TokenlogService;
import com.ldim.wsauth.util.MessageDecoder;
import com.ldim.wsauth.util.MessageEncoder;

@ServerEndpoint(value = "/service", encoders = { MessageEncoder.class }, decoders = { MessageDecoder.class })
public class ServiceEndpoint {

	@Inject
	TokenlogService service;

	@OnOpen
	@AuthenticateWithToken
	public void onOpen(Session session) {
		try {
			session.getBasicRemote().sendText("Connection established " + session.getId());
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	@OnMessage
	public void onMessage(Message message, Session session) {
		if (message.getType() == MessageType.CUSTOMER_REQUEST) {
			try {
				session.getBasicRemote().sendObject(service.getLog(message.getSequenceId()));
			} catch (IOException | EncodeException e) {
				throw new RuntimeException(e);
			}
		} else {
			try {
				session.getBasicRemote().sendObject(message);
			} catch (IOException | EncodeException e) {
				throw new RuntimeException(e);
			}
		}
	}

	@OnClose
	public void onClose(Session session) {
		System.out.println("Session " + session.getId() + " has ended");
	}

	@OnError
	@HandleError
	public void onError(Throwable e, Session session) {
	}
}
