package com.ldim.wsauth.entities;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;

@Entity
@NamedQueries({
    @NamedQuery(name = Customer.FIND_ALL, query = "SELECT c FROM Customer c"),
    @NamedQuery(name = Customer.FIND_BY_EMAIL, query = "SELECT c FROM Customer c WHERE c.email=:email"),
    @NamedQuery(name = Customer.FIND_BY_EMAIL_AND_PASSWORD, query = "SELECT c FROM Customer c WHERE c.email=:email AND c.password=:password")})
public class Customer {
	public static final String FIND_ALL = "Customer.findAll";
	public static final String FIND_BY_EMAIL = "Customer.findByEmail";
	public static final String FIND_BY_EMAIL_AND_PASSWORD = "Customer.findByEmailAndPassword";
	
	@Id @GeneratedValue
	private Long id;
	
	private String email;
	
	private String password;
	
	public Customer() {
		
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
	
}
