package com.ldim.wsauth.entities;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.ldim.wsauth.util.CustomDateSerializer;

@Entity
@NamedQueries({
    @NamedQuery(name = Tokenlog.FIND_ALL, query = "SELECT l FROM Tokenlog l")})
public class Tokenlog {
	public static final String FIND_ALL = "Tokenlog.findAll";
	
	@Id @GeneratedValue
	private Long id;
	
	private Date date;
	
	private String username;
	
	private String token;
	
	public Tokenlog() {
		
	}
	
	public Tokenlog(Date date, String username, String token) {
		this.date = date;
		this.username = username;
		this.token = token;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	@JsonSerialize(using = CustomDateSerializer.class)
	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}
	
}
