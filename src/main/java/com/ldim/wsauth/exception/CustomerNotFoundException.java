package com.ldim.wsauth.exception;

import com.ldim.wsauth.model.MessageType;

public class CustomerNotFoundException extends WebSocketException {
	private static final long serialVersionUID = 5169519166893128245L;

	public CustomerNotFoundException(String sequenceId) {
		super(MessageType.CUSTOMER_ERROR, "Customer not found", sequenceId);
	}

}
