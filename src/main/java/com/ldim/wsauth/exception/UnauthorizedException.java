package com.ldim.wsauth.exception;

import com.ldim.wsauth.model.MessageType;

public class UnauthorizedException extends WebSocketException {
	private static final long serialVersionUID = 5441491249786807428L;
	
	public UnauthorizedException(String sequenceId) {
		super(MessageType.CUSTOMER_ERROR, "Unauthorized", sequenceId);
	}
}
