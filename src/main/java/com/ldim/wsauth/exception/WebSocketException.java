package com.ldim.wsauth.exception;

import com.ldim.wsauth.model.MessageType;

public abstract class WebSocketException extends RuntimeException {
	private static final long serialVersionUID = 1L;
	private MessageType type;
	private String sequenceId;

	public WebSocketException(MessageType type, String message, String sequenceId) {
		super(message);
		this.type = type;
		this.sequenceId = sequenceId;
	}

	public MessageType getType() {
		return type;
	}

	public String getSequenceId() {
		return sequenceId;
	}

}
