package com.ldim.wsauth.model;

import java.util.Map;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "email",
        "password"
})
public class CredentialsData {
    private static final String PASSWORD = "password";
	private static final String EMAIL = "email";
	
	@JsonProperty(EMAIL)
    private String email;
    @JsonProperty(PASSWORD)
    private String password;

    public CredentialsData(Map<String, Object> props) {
    	this.email = (String) props.get(EMAIL);
    	this.password = (String) props.get(PASSWORD);
    }

	public String getEmail() {
		return email;
	}

	public String getPassword() {
		return password;
	}
}
