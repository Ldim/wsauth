package com.ldim.wsauth.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ErrorData {
	@JsonProperty("error_description")
	private String description;
	@JsonProperty("error_code")
	private String code;

	public ErrorData(String description, String code) {
		this.description = description;
		this.code = code;
	}
}
