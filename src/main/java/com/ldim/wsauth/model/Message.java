package com.ldim.wsauth.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "type",
        "sequence_id",
        "data"
})
public class Message {
	private static final String DATA = "data";
	private static final String SEQUENCE_ID = "sequence_id";
	private static final String TYPE = "type";
	
	@JsonProperty(TYPE)
    private MessageType type;
    @JsonProperty(SEQUENCE_ID)
    private String sequenceId;
    @JsonProperty(DATA)
    private Object data;
	
	@JsonCreator
	public Message(@JsonProperty(TYPE) MessageType type, @JsonProperty(SEQUENCE_ID) String sequenceId, @JsonProperty(DATA) Object data) {
		this.type = type;
		this.sequenceId = sequenceId;
		this.data = data;
	}
	
	public MessageType getType() {
		return type;
	}

	public String getSequenceId() {
		return sequenceId;
	}

	public Object getData() {
		return data;
	}
	
}
