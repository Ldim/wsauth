package com.ldim.wsauth.model;

public enum MessageType {
	LOGIN_CUSTOMER, CUSTOMER_REQUEST, CUSTOMER_API_TOKEN, CUSTOMER_ERROR, SERVER_ERROR
}
