package com.ldim.wsauth.model;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.ldim.wsauth.util.CustomDateSerializer;

public class TokenData {
	@JsonProperty("api_token")
	private String token;
	@JsonProperty("api_token_expiration_date")
	private Date expire;

	public TokenData(String token, long expire) {
		this.token = token;
		this.expire = new Date(expire);
	}

	public String getToken() {
		return token;
	}

	@JsonSerialize(using = CustomDateSerializer.class)
	public Date getExpire() {
		return expire;
	}
	
}
