package com.ldim.wsauth.repositories;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.ldim.wsauth.entities.Customer;

@Stateless
public class CustomerRepository {
	@PersistenceContext(unitName = "wsauthPU")
	private EntityManager em;

	public List<Customer> findAll() {
		return em.createNamedQuery(Customer.FIND_ALL, Customer.class).getResultList();
	}

	public List<Customer> findByEmail(String email) {
		return em.createNamedQuery(Customer.FIND_BY_EMAIL, Customer.class).setParameter("email", email).getResultList();
	}

	public List<Customer> findByEmailAndPassword(String email, String password) {
		return em.createNamedQuery(Customer.FIND_BY_EMAIL_AND_PASSWORD, Customer.class).setParameter("email", email)
				.setParameter("password", password).getResultList();
	}
}
