package com.ldim.wsauth.repositories;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.ldim.wsauth.entities.Tokenlog;

@Stateless
public class TokenlogRepository {
	@PersistenceContext(unitName = "wsauthPU")
	private EntityManager em;
	
	public List<Tokenlog> getAll() {
		return em.createNamedQuery(Tokenlog.FIND_ALL, Tokenlog.class).getResultList();
	}
	
	public void create(Tokenlog tokenlog) {
		em.merge(tokenlog);
	}
}
