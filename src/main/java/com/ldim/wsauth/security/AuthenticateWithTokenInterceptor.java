package com.ldim.wsauth.security;

import java.io.Serializable;
import java.util.UUID;

import javax.inject.Inject;
import javax.interceptor.AroundInvoke;
import javax.interceptor.Interceptor;
import javax.interceptor.InvocationContext;
import javax.websocket.Session;

import com.ldim.wsauth.exception.UnauthorizedException;
import com.ldim.wsauth.util.MessageHelper;

@Interceptor
@AuthenticateWithToken
public class AuthenticateWithTokenInterceptor implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@Inject
	TokenService tokenService;
	
	@Inject
	SessionsHandler sessionsHandler;
	
	@AroundInvoke
    public Object checkToken(InvocationContext ctx) throws Exception {
		Session session = (Session) ctx.getParameters()[0];
		String token = session.getQueryString();
		if (!tokenService.contains(token)) {
			session.getBasicRemote().sendObject(MessageHelper.error(new UnauthorizedException(UUID.randomUUID().toString())));
			session.close();
		} else {
			sessionsHandler.put(session, token);
		}
		return ctx.proceed();
	}

}
