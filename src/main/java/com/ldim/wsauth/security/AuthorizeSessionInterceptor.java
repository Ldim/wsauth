package com.ldim.wsauth.security;

import java.io.Serializable;

import javax.inject.Inject;
import javax.interceptor.AroundInvoke;
import javax.interceptor.Interceptor;
import javax.interceptor.InvocationContext;
import javax.websocket.Session;

import com.ldim.wsauth.exception.UnauthorizedException;
import com.ldim.wsauth.model.Message;
import com.ldim.wsauth.model.TokenData;

@Interceptor
@AuthorizeSession
public class AuthorizeSessionInterceptor implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@Inject
	SessionsHandler sessionsHandler; 
	
	@Inject
	DomainUsernamePasswordAuthenticationProvider domainUsernamePasswordAuthenticationProvider;

	@AroundInvoke
    public Object checkAuthorization(InvocationContext ctx) throws Exception {
		Message requestMessage = (Message) ctx.getParameters()[0];
		Session session = (Session) ctx.getParameters()[1];
		Message message = domainUsernamePasswordAuthenticationProvider.authenticate(requestMessage);
		if (message == null) {
			if (sessionsHandler.isValid(session)) {
				return ctx.proceed();
			} else {
				throw new UnauthorizedException(requestMessage.getSequenceId());
			}
		}
		sessionsHandler.put(session, ((TokenData) message.getData()).getToken());
		session.getBasicRemote().sendObject(message);
		return ctx.proceed();
	}

}
