package com.ldim.wsauth.security;

import java.util.Map;

import javax.ejb.Stateless;
import javax.inject.Inject;

import com.ldim.wsauth.exception.CustomerNotFoundException;
import com.ldim.wsauth.model.CredentialsData;
import com.ldim.wsauth.model.DomainUser;
import com.ldim.wsauth.model.Message;
import com.ldim.wsauth.model.MessageType;
import com.ldim.wsauth.util.MessageHelper;

@Stateless
public class DomainUsernamePasswordAuthenticationProvider {
	@Inject
	private TokenService tokenService;
	@Inject
	private TokenlogService tokenlogService;
	@Inject
	private ExternalAuthenticatorService externalAuthenticatorService;
	
	@SuppressWarnings("unchecked")
	public Message authenticate(Message message) {
		if (message.getType() == MessageType.LOGIN_CUSTOMER) {
			CredentialsData credentials = new CredentialsData((Map<String, Object>) message.getData());
			if (externalAuthenticatorService.authenticate(credentials.getEmail(), credentials.getPassword())) {
				String token = tokenService.generateNewToken();
				DomainUser user = new DomainUser(credentials.getEmail());
				long exp = tokenService.store(token, user);
				tokenlogService.write(user, token);
				return MessageHelper.token(message.getSequenceId(), token, exp);
			} else {
				throw new CustomerNotFoundException(message.getSequenceId());
			}
		}
		return null;
	}
}
