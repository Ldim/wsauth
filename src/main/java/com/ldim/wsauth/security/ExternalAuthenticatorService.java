package com.ldim.wsauth.security;

public interface ExternalAuthenticatorService {
	boolean authenticate(String username, String password);
}
