package com.ldim.wsauth.security;

import java.io.Serializable;
import java.util.UUID;

import javax.interceptor.AroundInvoke;
import javax.interceptor.Interceptor;
import javax.interceptor.InvocationContext;
import javax.websocket.Session;

import com.ldim.wsauth.exception.WebSocketException;
import com.ldim.wsauth.model.MessageType;
import com.ldim.wsauth.util.MessageHelper;

@Interceptor
@HandleError
public class HandleErrorInterceptor implements Serializable {
	private static final long serialVersionUID = -881883840548686248L;
	
	@AroundInvoke
	public Object processException(InvocationContext ctx) throws Exception {
		Throwable e = (Throwable) ctx.getParameters()[0];
		Session session = (Session) ctx.getParameters()[1];
		try {
			session.getBasicRemote().sendObject(MessageHelper.error((WebSocketException) e.getCause()));
		} catch (Exception e1) {
				session.getBasicRemote().sendObject(MessageHelper.error(MessageType.SERVER_ERROR,
						UUID.randomUUID().toString(), e.getMessage(), e.toString()));
		}
		return ctx.proceed();
	}

}
