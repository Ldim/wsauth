package com.ldim.wsauth.security;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.websocket.Session;

@Stateless
public class SessionsHandler {
	private static final String TOKEN = "token";
	
	@Inject
	TokenService tokenService;

	public void put(Session session, String token) {
		session.getUserProperties().put(TOKEN, token);
	}

	public boolean isValid(Session session) {
		return tokenService.contains((String) session.getUserProperties().get(TOKEN));
	}
}
