package com.ldim.wsauth.security;

import com.ldim.wsauth.model.DomainUser;

public interface TokenService {
	 String generateNewToken();
	 long store(String token, DomainUser domainUser);
	 boolean contains(String token);
	 DomainUser retrieve(String token);
}
