package com.ldim.wsauth.security;

import com.ldim.wsauth.model.DomainUser;
import com.ldim.wsauth.model.Message;

public interface TokenlogService {
	void write(DomainUser user, String token);
	Message getLog(String sequenceId);
}
