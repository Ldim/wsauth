package com.ldim.wsauth.util;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class AppProperties {
	private static final Properties props = new Properties();
	
	static {
		String propFileName = "config.properties";
		 
		InputStream inputStream = AppProperties.class.getClassLoader().getResourceAsStream(propFileName);

		if (inputStream != null) {
			try {
				props.load(inputStream);
			} catch (IOException e) {
				throw new RuntimeException(e);
			}
		}
	}
	
	private AppProperties() {
		
	}
	
	public static String getValue(String name) {
		return (String) props.get(name);
	}
}
