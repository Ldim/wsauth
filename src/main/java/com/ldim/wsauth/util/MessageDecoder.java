package com.ldim.wsauth.util;

import javax.websocket.DecodeException;
import javax.websocket.Decoder;
import javax.websocket.EndpointConfig;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.ldim.wsauth.model.Message;

public class MessageDecoder implements Decoder.Text<Message> {

	@Override
	public void init(EndpointConfig config) {
		
	}

	@Override
	public void destroy() {
		
	}

	@Override
	public Message decode(String s) throws DecodeException {
		try {
			return new ObjectMapper().readValue(s, Message.class);
		} catch (Exception e) {
			throw new DecodeException("decode", e.getMessage(), e);
		}
	}

	@Override
	public boolean willDecode(String s) {
		try {
			return new ObjectMapper().readValue(s, Message.class) != null;
		} catch (Exception e) {
			return false;
		}
	}

}
