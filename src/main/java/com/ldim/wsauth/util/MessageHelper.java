package com.ldim.wsauth.util;

import com.ldim.wsauth.exception.WebSocketException;
import com.ldim.wsauth.model.ErrorData;
import com.ldim.wsauth.model.Message;
import com.ldim.wsauth.model.MessageType;
import com.ldim.wsauth.model.TokenData;

public class MessageHelper {
	
	public static Message error(MessageType type, String sequence_id, String message, String error) {
		return new Message(type, sequence_id, new ErrorData(message, error));
	}
	
	public static Message error(WebSocketException e) {
		return new Message(e.getType(), e.getSequenceId(), new ErrorData(e.getMessage(), e.toString()));
	}
	
	public static Message token(String sequence_id, String token, long exp) {
		return new Message(MessageType.CUSTOMER_API_TOKEN, sequence_id, new TokenData(token, exp));
	}
}
